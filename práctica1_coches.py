
class Coche:
    def __init__(self, color, marca, modelo, matrícula, velocidad):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matrícula = matrícula
        self.velocidad = velocidad
#Creamos la clase coche, copn los atributos de acelerar y frenar
    def acelerar(self):
        self.velocidad = self.velocidad + 1
        return self
#Añadimos una unidad a la velocidad (acelerar)
    def frenar(self):
        self.velocidad = self.velocidad - 1
        return self
#Restamos una unidad a la velocidad (frenar)

coche1 = Coche("azul","Ford","Fiesta","9667FMF",50)
coche2 = Coche("blanco","Nissan","Qashqai","2441JKH",120)
#Creo un par de choches para probar el programa 

print(coche1.color, coche1.marca,coche1.modelo,coche1.matrícula,coche1.velocidad)
#Esto es lo que me imprime el programa


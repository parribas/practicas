import unittest
from práctica1_coches import Coche

class TestCoche(unittest.TestCase):
    def test_ac(self):
        cochetest1 = Coche("azul","Ford","Fiesta","9667FMF",50)
        ac = cochetest1.acelerar()
        self.assertEqual(ac.velocidad, 51)
#Probamos que al acelerar se añada una unidad a la velocidad

    def test_fr(self):
        cochetest2 = Coche("blanco","Nissan","Qashqai","2441JKH",120)
        fr = cochetest2.frenar()
        self.assertEqual(fr.velocidad, 119)
#Probamos que al frenar le reste una unidad a la velocidad

if __name__ == '__main__':
    unittest.main()
